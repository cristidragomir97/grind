import os, sys, re
import winshell
import platform
import win32gui
import psutil
import win32process
import uiautomation as automation
from datetime import datetime

os = platform.system()


def get_apps():
    apps = []
    shortcuts = {}

    user_programs = winshell.programs()
    for dirpath, dirnames, filenames in os.walk(user_programs):
        relpath = dirpath[1 + len(user_programs):]
        shortcuts.setdefault(
            relpath, []
        ).extend(
            [winshell.shortcut(os.path.join(dirpath, f)) for f in filenames]
        )

    all_programs = winshell.programs(common=1)
    for dirpath, dirnames, filenames in os.walk(all_programs):
        relpath = dirpath[1 + len(all_programs):]
        shortcuts.setdefault(
            relpath, []
        ).extend(
            [winshell.shortcut(os.path.join(dirpath, f)) for f in filenames]
        )

    for relpath, lnks in sorted(shortcuts.items()):
        level = relpath.count("\\")
        if level == 0:
            print("")

        for lnk in lnks:
            path = lnk.path
            name, _ = os.path.splitext(os.path.basename(lnk.lnk_filepath))

            if "exe" in path:
                exe = path.split("\\")[::-1][0]
                apps.append({"name": name, "exe": exe})

    return apps

def get_snapshot():
    at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    w = win32gui
    title = win32gui.GetWindowText(w.GetForegroundWindow())
    pid = win32process.GetWindowThreadProcessId(w.GetForegroundWindow())
    app = psutil.Process(pid[-1]).name()
    url = get_url(app)

    if url is None:
        return {"at": at, "title": title, "app": app, "url": None}
    else:
        return {"at": at, "title": title, "app": app, "url": url}

def get_url(app):
    control = automation.GetFocusedControl()
    controlList = []
    while control:
        controlList.insert(0, control)
        control = control.GetParentControl()

    if len(controlList) == 1:
        control = controlList[0]
    else:
        control = controlList[1]

    if (app == "chrome.exe"):
        return automation.FindControl(control, lambda c, d: isinstance(c,automation.EditControl) and "Address and search bar" in c.Name).CurrentValue()
    if (app == "mozilla.exe"):
        return automation.FindControl(control, lambda c, d: isinstance(c,automation.EditControl) and "Address and search bar" in c.Name)
    else:
        return None