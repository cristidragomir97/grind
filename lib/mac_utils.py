import os, json, datetime
from AppKit import NSWorkspace
import appscript
import Quartz




def get_apps():
    thefuckisthis = os.popen("find /Applications -maxdepth 2 -iname *.app").read().split('\n')
    return thefuckisthis


def get_snapshot():
    obj = {}
    _now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    curr_pid = NSWorkspace.sharedWorkspace().activeApplication()['NSApplicationProcessIdentifier']


    for window in Quartz.CGWindowListCopyWindowInfo(Quartz.kCGWindowListOptionOnScreenOnly, Quartz.kCGNullWindowID):
        app = window['kCGWindowOwnerName']
        document = window.get('kCGWindowName', u'Unknown').encode('ascii', 'ignore')
        pid = window['kCGWindowOwnerPID']

        url = ""

        if app == "Google Chrome":
            url = appscript.app('Google Chrome').windows.first.active_tab.URL()
            obj = {'at': _now, 'app': app, 'document': str(document.decode('utf-8')), 'url':url}

        elif app == "Safari":
            url = appscript.app('Safari').windows.first.current_tab.URL()
            obj = {'at': _now, 'app': app, 'document': str(document.decode('utf-8')), 'url':url}

        else:
            obj = {'at': _now, 'app': app, 'document': str(document.decode('utf-8')), 'url':''}


        if pid == curr_pid and document is not b'':
            break


    return obj










