from lib import _utils
import time
from threading import Thread, Condition
from tinydb import TinyDB, Query

SNAPSHOT_RATE = 10
DB_RATE = 100
TRACK_EVERYTHING = True

queue = []
condition = Condition()





class SnapshotThread(Thread):
    def run(self):
        global queue

        while True:
                queue.append(_utils.get_snapshot())
                time.sleep(SNAPSHOT_RATE)




class DatabaseThread(Thread):
    def run(self):
        while True:
            global queue

            for element in list(queue):
                if TRACK_EVERYTHING:


                    app_item = {'table': 'raw',
                                'at': element['at'],
                                'app': element['app'],
                                'document': element['document'],
                                'url': element['url']
                    }

                    print("[-] logging {}".format(app_item))
                    database.insert(app_item)

            queue = []

            time.sleep(DB_RATE)





if __name__ == '__main__':
    database = TinyDB("../storage/grind.json")

    db_thread = DatabaseThread()
    db_thread.start()

    snapshot_thread = SnapshotThread()
    snapshot_thread.start()












