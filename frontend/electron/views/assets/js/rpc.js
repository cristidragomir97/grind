const zerorpc = require("zerorpc")
let client = new zerorpc.Client()
client.connect('tcp://127.0.0.1:4242')

client.invoke("echo", "muie la muisti", (error, res) => {
  if(error || res !== 'muie la muisti') {
    console.error(error)
  } else {
    console.log("server is ready")
  }
})