const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const path = require('path')


let mainWindow = null

const createWindow = () => {
  mainWindow = new BrowserWindow({width: 1280, height:720})
  mainWindow.setMenu(null)
   mainWindow.webContents.openDevTools()

  mainWindow.loadURL(require('url').format({
    pathname: path.join(__dirname, '/views/settings.html'),
    protocol: 'file:',
    slashes: true
  }))


  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
