from __future__ import print_function
import zerorpc
import subprocess, os, sys, json
#from lib import win_utils as _win
from lib import _utils
from tinydb import TinyDB, Query


ADDR = 'tcp://127.0.0.1:4242'

class Frontend(object):
    def echo(self, text):
        print(text)

    def list_apps(self):
        proper_json = []

        stuff = _utils.get_apps()

        stuff = stuff[::-1]

        for element in stuff:
            path = element
            runnable = element.split('/')[::-1][0]
            name = runnable.split('.')[0]
            proper_json.append({
                'name': name,
                'runnable': runnable,
                'path': path
            })

        return json.dumps(proper_json)

    def get_filtered(self):
        object = Query()
        apps = database.search((object.table == 'apps'))
        return json.dumps(apps)

    def get_activity(self):
        object = Query()
        apps = database.search((object.table == 'raw'))
        return json.dumps(apps)

    def save_app(self, app_json):
        app_obj = json.loads(app_json)

        app_item = {'table': 'apps',
                    'name': app_obj['name'],
                    'runnable': app_obj['runnable'],
                    'path': app_obj['path']}

        database.insert(app_item)

        return True







def start_rpc(addr):
    s = zerorpc.Server(Frontend())
    s.bind(addr)
    print('> rpc server running {}'.format(addr))
    s.run()

if __name__ == '__main__':
    database = TinyDB("../storage/grind.json")
    subprocess.Popen(['node', 'electron/node_modules/electron/cli.js', 'electron/main.js'])

    start_rpc(ADDR)





